<?php
session_start();
//error_reporting(0);
if (empty($_SESSION['username']) AND empty($_SESSION['passuser'])){
  echo "<link href='style.css' rel='stylesheet' type='text/css'>
 <center>Untuk mengakses modul, Anda harus login <br>";
  echo "<a href=../../index.php><b>LOGIN</b></a></center>";
}
else{
include "../../config/koneksi.php";
include "../../config/library.php";
include "../../config/fungsi_thumb.php";

$module=$_GET[module];
$act=$_GET[act];

// Hapus Data Buku
if ($module=='product' AND $act=='hapus'){
  $data=mysqli_fetch_array(mysqli_query($conn,"SELECT picture FROM product WHERE id='$_GET[id]'"));
  if ($data['picture']!=''){
     mysqli_query($conn,"DELETE FROM product WHERE id='$_GET[id]'");
     unlink("../../gambar_produk/$_GET[namafile]");   
     unlink("../../gambar_produk/small_$GET[namafile]");   
  }
  else{
     mysqli_query($conn,"DELETE FROM product WHERE id='$_GET[id]'");
  }
  header('location:../../cms.php?module='.$module);
}

// Input Data Buku
elseif ($module=='product' AND $act=='input'){
  $lokasi_file    = $_FILES['fupload']['tmp_name'];
  $tipe_file      = $_FILES['fupload']['type'];
  $nama_file      = $_FILES['fupload']['name'];
  $acak           = rand(1,99);
  $nama_file_unik = $acak.$nama_file; 

// Apabila ada gambar yang diupload
  if (!empty($lokasi_file)){
    if ($tipe_file != "image/jpeg" AND $tipe_file != "image/pjpeg"){
    echo "<script>window.alert('Upload Gagal, Pastikan File yang di Upload bertipe *.JPG');
        window.location=('../../cms.php?module=product)</script>";
    }
    else{
    UploadImage($nama_file_unik);

    mysqli_query($conn,"INSERT INTO product(name_product,
                                    descriptions,
                                    picture) 
                            VALUES('$_POST[nama]',
                                   '$_POST[deskripsi]',
                                   '$nama_file_unik')");
  header('location:../../cms.php?module='.$module);
  }
  }
  else{
    mysqli_query($conn,"INSERT INTO product(name_product,
                                    descriptions) 
                            VALUES('$_POST[nama]',
                                   '$_POST[deskripsi]')");
  header('location:../../cms.php?module='.$module);
  }
}

// Update data buku
elseif ($module=='product' AND $act=='update'){
  $lokasi_file    = $_FILES['fupload']['tmp_name'];
  $tipe_file      = $_FILES['fupload']['type'];
  $nama_file      = $_FILES['fupload']['name'];
  $acak           = rand(1,99);
  $nama_file_unik = $acak.$nama_file; 
  // Apabila gambar tidak diganti
  if (empty($lokasi_file)){
    mysqli_query($conn,"UPDATE product SET name_product       = '$_POST[nama]',
                                    descriptions   = '$_POST[deskripsi]'
                             WHERE id   = '$_POST[id]'");
  header('location:../../cms.php?module='.$module);
  }
  else{
    if ($tipe_file != "image/jpeg" AND $tipe_file != "image/pjpeg"){
    echo "<script>window.alert('Upload Gagal, Pastikan File yang di Upload bertipe *.JPG');
        window.location=('../../cms.php?module=product')</script>";
    }
    else{
    UploadImage($nama_file_unik);
    mysqli_query($conn,"UPDATE product SET name_product = '$_POST[nama]',
                                   descriptions   = '$_POST[deskripsi]',
                                   picture      = '$nama_file_unik'   
                             WHERE id   = '$_POST[id]'");
   header('location:../../cms.php?module='.$module);
   }
  }
}
}
?>
