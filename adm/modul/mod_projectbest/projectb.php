<?php
$aksi="modul/mod_projectbest/aksi_projectb.php";
switch($_GET[act]){
   //Tampil data kabupaten
  default:
    echo"<h1>Data Project Terbaik</h1>
	<div class='panel panel-default'>
    <div class='panel-heading'>        
        <form class='form-inline'>
            <div class='form-group'>
           <a class='btn btn-primary' onclick=\"window.location.href='?module=projectb&act=tambahprjb';\"><span class='glyphicon glyphicon-plus'></span> Tambah</a>
            </div>
        </form>
		</div>
    <table class='table table-bordered table-hover table-striped'>
    <thead>
        <tr>
            <th>No</th>
            <th>Nama Project</th>
            <th>Gambar</th>
            <th>Tahun</th>
			 <th>Aksi</th>
        </tr>
    </thead>";
		
    $tampil=mysqli_query($conn,"SELECT * FROM project_best ORDER BY best_project");
    $no=1;
    while ($r=mysqli_fetch_array($tampil)){
       echo "<tr><td>$no</td>
          <td>$r[best_project]</td>
          <td><img src='gambar_project/small_$r[picture]' alt=''></td>
             <td>$r[years]</td>
             <td><a class='btn btn-xs btn-warning' href=?module=projectb&act=editprjb&id=$r[id]><span class='glyphicon glyphicon-edit'></span></a> 
	               <a class='btn btn-xs btn-danger' href=$aksi?module=projectb&act=hapus&id=$r[id]><span class='glyphicon glyphicon-trash'></span></a>

             </td></tr>";
      $no++;
    }
    echo "</table></div>";
    break;
  
  // Form Tambah Kota/Kabupaten
  case "tambahprjb":
  echo "<h1>Tambah Data Project Terbaik</h1>
<div class='row'>
<div class='col-sm-6'>
<form method=POST action='$aksi?module=projectb&act=input'>

<div class='form-group'>
    <label>Pilih Project Terbaik<span class='text-danger'>*</span></label>
    <select class='form-control' name='prjb'>
           <option value=0 selected>- Pilih Project -</option>";
            $tampil=mysqli_query($conn,"SELECT * FROM project ORDER BY name_project");
            while($r=mysqli_fetch_array($tampil)){
            echo" <option value='$r[name_project]'>$r[name_project]</option>";
            }
	 echo" </select>
</div>

<div class='form-group'>
    <label>Pilih Tahun<span class='text-danger'>*</span></label>
    <select class='form-control' name='thn'>
           <option value=0 selected>- Tahun -</option>";
            $tampil=mysqli_query($conn,"SELECT * FROM tahun ORDER BY project_year");
            while($r=mysqli_fetch_array($tampil)){
            echo" <option value='$r[project_year]'>$r[project_year]</option>";
            }
	 echo" </select>
</div>

<div class='form-group'>
    <label>Gambar<span class='text-danger'></span></label>
    <input class='form-control' type=file name='fupload' value=''/>
</div>

<button class='btn btn-primary'><span class='glyphicon glyphicon-save'></span> Simpan</button>
<a class='btn btn-danger' onclick=self.history.back() ><span class='glyphicon glyphicon-arrow-left'></span> Kembali</a>
</form>
</div>
</div>";

break;

// Form Edit kabupaten 
  case "editprjb":
  $edit=mysqli_query($conn,"SELECT * FROM project_best WHERE id='$_GET[id]'");
  $r=mysqli_fetch_array($edit);
  echo "<h1>Edit Data Project Terbaik</h1>
<div class='row'>
<div class='col-sm-6'>
<form method=POST action=$aksi?module=projectb&act=update>
<input type=hidden name=id value='$r[id]'>

<div class='form-group'>
    <label>Edit Project Terbaik<span class='text-danger'>*</span></label>
    <select class='form-control' name='prjb'>";
         
		  $tampil=mysqli_query($conn,"SELECT * FROM project ORDER BY name_project");
          if ($r[best_project]==0){
            echo "<option value=0 selected>- Pilih Project -</option>";
          }   

          while($w=mysqli_fetch_array($tampil)){
            if ($r[best_project]==$w[name_project]){
              echo "<option value='$w[name_project]' selected>$w[name_project]</option>";
            }
            else{
              echo "<option value='$w[name_project]'>$w[name_project]</option>";
            }
          }

	 echo" </select>
</div>

<div class='form-group'>
    <label>Edit Tahun<span class='text-danger'>*</span></label>
    <select class='form-control' name='thn'>";
         
		  $tampil=mysqli_query($conn,"SELECT * FROM tahun ORDER BY project_year");
          if ($r[years]==0){
            echo "<option value=0 selected>- Tahun -</option>";
          }   

          while($w=mysqli_fetch_array($tampil)){
            if ($r[years]==$w[project_year]){
              echo "<option value='$w[project_year]' selected>$w[project_year]</option>";
            }
            else{
              echo "<option value='$w[project_year]'>$w[project_year]</option>";
            }
          }

	 echo" </select>
</div>

<div class='form-group'>
    <label>Gambar<span class='text-danger'></span></label>
    <input class='form-control' type=file name='fupload'/>
</div>

<button class='btn btn-primary'><span class='glyphicon glyphicon-save'></span> Simpan</button>
<a class='btn btn-danger' onclick=self.history.back() ><span class='glyphicon glyphicon-arrow-left'></span> Kembali</a>
</form>
</div>
</div>";
  
  break;
}  

  
	?>
 