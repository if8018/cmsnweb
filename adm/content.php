<?php
include "config/koneksi.php";
include "config/library.php";
include "config/fungsi_indotgl.php";
include "config/fungsi_combobox.php";
include "class_paging.php";

// Bagian Home
if ($_GET['module']=='home'){
  if ($_SESSION['leveluser']=='admin'){
	
?>

<h1 class="entry-title">CMS - PT. Hasdi Mustika Utama</h1>
<div class="entry-body"><br><br>
  <p><b>Master/Produk -</b> Untuk tambah, ubah, dan hapus produk </p><br>
  <p><b>Master/Project -</b> Untuk tambah, ubah dan hapus project </p><br>
  <p><b>Master/Project Terbaik -</b> Untuk menambahkan project- project unggulan kedalam slideshow </p><br>
  <p><b>Master/Klien Kategori -</b> Untuk mengatur kategori klien </p><br>
  <p><b>Master/Klien -</b> Untuk tambah, ubah, dan hapus klien per kategori </p><br>
  <p><b>Pesan -</b> Untuk melihat pesan dari contact form </p><br>
  <p><b>Utility/Ganti Banner -</b> Untuk mengganti banner pada hero image </p><br>
  <p><b>Utility/Ganti Password -</b> Untuk mengganti password admin login </p><br>
</div>		

<?php
	}
}
// Bagian Ganti Password
elseif ($_GET[module]=='password'){
  if ($_SESSION['leveluser']=='admin'){
    include "modul/mod_password/password.php";
  }
}

// Bagian Master Buku
elseif ($_GET[module]=='product'){
  if ($_SESSION['leveluser']=='admin'){
    include "modul/mod_buku/buku.php";
  }
}

// Bagian Propinsi
elseif ($_GET[module]=='propinsi'){
  if ($_SESSION['leveluser']=='admin'){
    include "modul/mod_propinsi/propinsi.php";
  }
}

// Bagian Propinsi
elseif ($_GET[module]=='kabupaten'){
  if ($_SESSION['leveluser']=='admin'){
    include "modul/mod_kabupaten/kabupaten.php";
  }
}

elseif ($_GET[module]=='project'){
  if ($_SESSION['leveluser']=='admin'){
    include "modul/mod_projects/project.php";
  }
}
elseif ($_GET[module]=='projectb'){
  if ($_SESSION['leveluser']=='admin'){
    include "modul/mod_projectbest/projectb.php";
  }
}
elseif ($_GET[module]=='pesan'){
  if ($_SESSION['leveluser']=='admin'){
    include "modul/mod_pesan/pesan.php";
  }
}
elseif ($_GET[module]=='banner'){
  if ($_SESSION['leveluser']=='admin'){
    include "modul/mod_banner/banner.php";
  }
}

// Apabila modul tidak ditemukan
else{
  echo "<p><b>MODUL BELUM ADA ATAU BELUM LENGKAP</b></p>";
}
?>
