<!DOCTYPE html>
<html>
    <head>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width"><link rel='stylesheet' href='https://npmcdn.com/flickity@2/dist/flickity.css'>
        <link rel="stylesheet" type="text/css" href="project.css">
        <TItle> PT. Hasdi Mustika Utama 2019</TItle>
    </head>
    <header>
        
    </header>
    <body>
        <div class="header" style="position: relative;">
            <div class="logo">
                <img class="logo"src="assets/logo-01.jpg" alt="logo">
            </div>
            <div class="header-right" id="myTopnav">
                <a href="index.php">HOME</a>
                <a href="articles.php">ARTICLES</a>
                <a href="client.php">CLIENT</a>
                <a href="product.php">PRODUCT</a>
                <a href="#" class="active">PROJECT</a>
                <a href="about.php">ABOUT US</a>
                <a href="contact.php">CONTACT US</a>
                <a href="javascript:void(0);" style="font-size:1.1vw;" class="icon" onclick="myFunction()">&#9776;</a>
            </div>
        </div>
        <div class="content" style="padding: 2% 17%;">
            <h1>PROJECT</h1>
            <div class="button-row">
                <div class="button-group">
                    <?php
                    include "adm/config/koneksi.php";
                    $tampil=mysqli_query($conn,"SELECT * FROM tahun ORDER BY project_year DESC");
                    $no=1;
                    while ($r=mysqli_fetch_array($tampil))	
                    {?>
                    <button class="button"><?php echo $r['project_year'];?></button>
                    <?php
                    $no++;
                    }?>
                </div>
            </div>
            <div class="carousel">
                <?php
                include "adm/config/koneksi.php";
                $tampil=mysqli_query($conn,"SELECT * FROM project_best WHERE years='2014' ORDER BY id DESC");
                $no=1;
                while ($r=mysqli_fetch_array($tampil))	
                {?>
                <div class="carousel-cell">
                <img src="adm/gambar_project/<?php echo $r['picture'];?>" alt="">
                    <div class="caro">
                        <h1><?php echo $r['best_project'];?></h1>
                        <p><?php echo $r['years'];?></p>
                    </div>
                </div>
                <?php
                $no++;
                }?>
            </div>
<br><br><br><br><br>
            <div class="all-projects"> 
            <?php
            include "adm/config/koneksi.php";
            $tampil=mysqli_query($conn,"SELECT * FROM project WHERE years='2014' ORDER BY id DESC");
            $no=1;
            while ($r=mysqli_fetch_array($tampil))	
            {?>
            - <?php echo $r['name_project'];?><br/>
            <?php
            $no++;
            }?>
            </div>
        </div>
            
        <script src='https://code.jquery.com/jquery-3.1.0.min.js'></script>
        <script src='https://npmcdn.com/flickity@2/dist/flickity.pkgd.js'></script>
        <script  src="project.js"></script>
    </body>
    <footer>
        <img src="assets/HASDI-10.jpg" alt="">
    </footer>
    </html>