<!DOCTYPE html>
<html>
    <head>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width"><link rel='stylesheet' href='https://npmcdn.com/flickity@2/dist/flickity.css'>
        <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
        <link rel="stylesheet" type="text/css" href="styles.css">
        <TItle> PT. Hasdi Mustika Utama 2019</TItle>
    </head>
    <header>
        
    </header>
    <body>
        <div class="header" style="position: relative;">
            <div class="logo">
                <img class="logo"src="assets/logo-01.jpg" alt="logo">
            </div>
            <div class="header-right" id="myTopnav">
                <a href="index.php">HOME</a>
                <a href="articles.php">ARTICLES</a>
                <a href="client.php">CLIENT</a>
                <a href="product.php">PRODUCT</a>
                <a href="project.php">PROJECT</a>
                <a href="#" class="active">ABOUT US</a>
                <a href="contact.php">CONTACT US</a>
                <a href="javascript:void(0);" style="font-size:1.1vw;" class="icon" onclick="myFunction()">&#9776;</a>
            </div>
        </div>
        <div class="content" style="padding: 2% 17%; line-height: 2;font-family: 'AvenirNextLTPro-Regular';font-weight: 600;color: gray; font-size: 1.2vw;">
            <h1>ABOUT US</h1><br>
            Berdiri sejak tahun 1978, PT. Hasdi Mustika Utama adalah distributor plywood dan pintu Homen di Indonesia<br> 
            yang telah memenuhi dan berpredikat standard ISO Management Mutu 9001. Hingga saat ini PT Hasdi Mustika <br> 
            Utama telah banyak bekerjasama dengan Perusahaan-Perusahaan Swasta, Asing maupun BUMN terutama <br> 
            dalam hal pengadaan Plywood dan Pintu baja bermutu untuk pembangunan <br> 
            berbagai Mega proyek di indonesia.
            <br><br>
            Pada tahun 2007, PT. Hasdi Mustika Utama telah mendapatkan sertifikat ISO 9001 –   <br>  
            Quality Management System yang menunjukkan keseriusan dan komitmen kami dalam hal :<br><br>
            - Kestabilan dalam mengolah produk bermutu.<br>
            - Pengiriman dalam 1 hari.<br>
            - Respon yang cepat dalam memberikan pelayanan.<br><br>
            <img src="assets/certificate.jpg" alt=""> <br><br>
            <h1>VISI :</h1><br>

            Distributor bertaraf internasional yang terpercaya dalam produk dan pelayanan untuk meningkatkan 
            kesejahterakan masyarakat.<br> <br> 

            <h1>MISI :</h1><br>

            - Melayani customer dengan komitmen, sportif, innovative, creative dan positif dalam menjalankan usahanya.<br>
            - Melayani customer dengan suara hati secara professional.<br>
            - Terdiri atas team yang high spirit, progresif, konsisten, solidaritas dan bertanggung jawab dalam<br>  
            melaksanakan pekerjaannya.<br>
            - Menciptakan tenaga professional yang berkualitas, bertanggung jawab dan berdedikasi tinggi.<br>
            - Mendukung dan menciptakan kelestarian alam dan lingkungan sesuai program pemerintah mengenai <br> 
            ramah lingkungan.<br> <br> 

            <h1>BUDAYA :</h1><br>
            
            1. <b>H</b>ARD WORKING & <b>H</b>ONEST<br>
            Bekerja keras dengan cerdas dan jujur untuk mencapai tujuan perusahaan.<br>
            2. <b>A</b>TTITUDE & <b>A</b>PPRECIATE<br>
            Mendapatkan apresiasi yang baik dari customer melalui sikap dan etika bisnis yang baik.<br>
            3. <b>S</b>PIRIT & <b>S</b>PORTIF<br>
            Semangat kerja yang disertai dengan pemikiran positif akan membuahkan hasil maksimal dan konsisten.<br>
            4. <b>D</b>EVELOPMENT & <b>D</b>EDICATION<br>
            Dengan dedikasi tinggi kita mampu membawa perusahaan mencapai tujuan.<br>
            5. <b>I</b>NOVATIF & <b>I</b>NTEGRITY<br>
            Integritas yang tinggi dan inovasi yang terus menerus membawa perusahaan menuju taraf internasional.<br>
        </div>
        <script src='https://code.jquery.com/jquery-3.1.0.min.js'></script>
        <script src='https://npmcdn.com/flickity@2/dist/flickity.pkgd.js'></script>
        <script  src="script.js"></script>
    </body>
    <footer>
        <img src="assets/HASDI-10.jpg" alt="">
    </footer>
    </html>