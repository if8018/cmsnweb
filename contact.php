<!DOCTYPE html>
<html>
    <head>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width"><link rel='stylesheet' href='https://npmcdn.com/flickity@2/dist/flickity.css'>
        <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
        <link rel="stylesheet" type="text/css" href="styles.css">
        <TItle> PT. Hasdi Mustika Utama 2019</TItle>
    </head>
    <header>
        
    </header>
    <body>
        <div class="header" style="position: relative;">
            <div class="logo">
                <img class="logo"src="assets/logo-01.jpg" alt="logo">
            </div>
            <div class="header-right" id="myTopnav">
                <a href="index.php">HOME</a>
                <a href="articles.php">ARTICLES</a>
                <a href="client.php">CLIENT</a>
                <a href="product.php">PRODUCT</a>
                <a href="project.php">PROJECT</a>
                <a href="about.php">ABOUT US</a>
                <a href="#" class="active">CONTACT US</a>
                <a href="javascript:void(0);" style="font-size:1.1vw;" class="icon" onclick="myFunction()">&#9776;</a>
            </div>
        </div>
        <div class="content" style="padding: 2% 17%;">
            <h1>CONTACT US</h1> <br><br>
            <h1 style="font-size:2vw; color: black;">PT. Hasdi Mustika Utama</h1>
            <p style="font-size:1.3vw;">Jl Sunter Agung Utara Raya Blok D1/5B Jakarta 14350</p><br><br>
            
            <div>
            <div style="width: 45%; float: left;">
                <i class="fa fa-phone" aria-hidden="true" style="float: left; font-size:2.5vw;"></i>
                <div style="float: left; padding-left: 5%; font-size:1.3vw;">
                    <p style="line-height: 1.3vw;">+62 21 65307331</p>
                    <p style="line-height: 1.3vw;">+62 21 65307333</p>
                    <p style="line-height: 1.3vw;">+62 21 65307330</p><br>
                </div>
            </div>
            <div style="width: 45%; float: left;">
                <i class="fa fa-envelope" aria-hidden="true" style="float: left; font-size:2vw;"></i>
                <div style="float: left; padding-left: 5%;font-size:1.3vw;">
                    <p style="line-height: 1.3vw;">marketing@hasdimustika.com</p>
                </div>
            </div>
            <div style="width: 100%; float: left;">
                <p style="font-size:1.5vw;">Office Hours :</p>
                <p style="line-height: 1.3vw;">- Monday - Friday : 8am - 5pm</p>
                <p style="line-height: 1.3vw;">- Saturday : 8am - 3pm</p><br><br>
            </div>
            <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3966.941220311166!2d106.86202841450151!3d-6.138599361889064!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x2e69f57f7eb11fab%3A0xde1a17fd5d1ea1d9!2sPT.%20Hasdi%20Mustika%20Utama!5e0!3m2!1sid!2sid!4v1567401589535!5m2!1sid!2sid" frameborder="0" style="border:0;" allowfullscreen="">
            </iframe>
            <br/><br/><br/>
            
            <form method="post" action="send.php">
            <div class="form-row">
                <div class="form-group col-md-5">
                <label>Your Name</label>
                <input class="form-control" name="sendername" style="background-color: #f2f2f2; border: none; height: 4vw;">
                </div>
                <div class="form-group col-md-2">
                </div>
                <div class="form-group col-md-5">
                <label>Your Email</label>
                <input class="form-control" name="sendermail" style="background-color: #f2f2f2; border: none; height: 4vw;">
                </div>
            </div>
            <div class="form-row">
                <div class="form-group col-md-5">
                <label>Your Phone</label>
                <input class="form-control" name="senderphone"style="background-color: #f2f2f2; border: none; height: 4vw;">
                </div>
                <div class="form-group col-md-2">
                </div>
                <div class="form-group col-md-5">
                <label>Subject</label>
                <input class="form-control" name="sendersub" style="background-color: #f2f2f2; border: none; height: 4vw;">
                </div>
            </div>
            <div class="form-group">
                <label>Your Message</label>
                <textarea class="form-control" name="sendermsg" rows="10" style="background-color: #f2f2f2; border: none;"></textarea>
            </div>
            <button type="submit" class="btn btn-primary col-md-2" style="background-color: #f2f2f2; border: none; color: black; height: 4vw; display: block; margin-left: auto; margin-right: auto;">Send</button>
            </form>
        </div>
        <script src='https://code.jquery.com/jquery-3.1.0.min.js'></script>
        <script src='https://npmcdn.com/flickity@2/dist/flickity.pkgd.js'></script>
        <script  src="script.js"></script>
    </body>
    <footer>
        <img src="assets/HASDI-10.jpg" alt="">
    </footer>
    </html>