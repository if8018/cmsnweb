-- phpMyAdmin SQL Dump
-- version 4.9.0.1
-- https://www.phpmyadmin.net/
--
-- ホスト: 127.0.0.1
-- 生成日時: 
-- サーバのバージョン： 10.4.6-MariaDB
-- PHP のバージョン: 7.2.22

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- データベース: `hasdi`
--

-- --------------------------------------------------------

--
-- テーブルの構造 `about`
--

CREATE TABLE `about` (
  `id` int(2) NOT NULL,
  `company` varchar(50) NOT NULL,
  `email` varchar(50) NOT NULL,
  `alamat` varchar(250) NOT NULL,
  `hp` varchar(50) NOT NULL,
  `descriptions` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- テーブルのデータのダンプ `about`
--

INSERT INTO `about` (`id`, `company`, `email`, `alamat`, `hp`, `descriptions`) VALUES
(1, 'hasdi', 'yu801817@gmail.com', 'Jl Sunter Agung Utara Raya Blok D1/5B Jakarta 14350', '02165307331', 'Berdiri sejak tahun 1978, PT. Hasdi Mustika Utama adalah distributor plywood dan pintu Homen di Indonesia yang telah memenuhi dan berpredikat standard ISO Management Mutu 9001. Hingga saat ini PT Hasdi Mustika Utama telah banyak bekerjasama dengan Perusahaan-Perusahaan Swasta, Asing maupun BUMN terutama dalam hal pengadaan Plywood dan Pintu baja bermutu untuk pembangunan berbagai Mega proyek di indonesia.');

-- --------------------------------------------------------

--
-- テーブルの構造 `admins`
--

CREATE TABLE `admins` (
  `username` varchar(50) COLLATE latin1_general_ci NOT NULL,
  `password` varchar(50) COLLATE latin1_general_ci NOT NULL,
  `nama_lengkap` varchar(100) COLLATE latin1_general_ci NOT NULL,
  `email` varchar(100) COLLATE latin1_general_ci NOT NULL,
  `no_telp` varchar(20) COLLATE latin1_general_ci NOT NULL,
  `level` varchar(20) COLLATE latin1_general_ci NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci;

--
-- テーブルのデータのダンプ `admins`
--

INSERT INTO `admins` (`username`, `password`, `nama_lengkap`, `email`, `no_telp`, `level`) VALUES
('admin', '21232f297a57a5a743894a0e4a801fc3', 'Administrator', 'admin@admin.com', '085691638906', 'admin');

-- --------------------------------------------------------

--
-- テーブルの構造 `banner`
--

CREATE TABLE `banner` (
  `id` int(2) NOT NULL,
  `names` varchar(100) NOT NULL,
  `picture` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- テーブルのデータのダンプ `banner`
--

INSERT INTO `banner` (`id`, `names`, `picture`) VALUES
(1, 'banner1', 'HASDI-12.jpg');

-- --------------------------------------------------------

--
-- テーブルの構造 `category_client`
--

CREATE TABLE `category_client` (
  `kode` int(2) NOT NULL,
  `name_category` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- テーブルのデータのダンプ `category_client`
--

INSERT INTO `category_client` (`kode`, `name_category`) VALUES
(1, 'Goverment Contractor'),
(3, 'Interior Contractors'),
(4, 'Private Contractors');

-- --------------------------------------------------------

--
-- テーブルの構造 `client`
--

CREATE TABLE `client` (
  `id` int(6) NOT NULL,
  `name_client` varchar(50) NOT NULL,
  `category` int(2) NOT NULL,
  `picture` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- テーブルのデータのダンプ `client`
--

INSERT INTO `client` (`id`, `name_client`, `category`, `picture`) VALUES
(1, 'Wika', 1, 'awika.jpg'),
(2, 'Dinamika', 3, 'dinamika.png'),
(3, 'Tata', 4, 'tata.jpg'),
(10, 'Adhi', 1, 'adhi.jpg'),
(11, 'HK', 1, 'hk.jpg'),
(12, 'Istana Karya', 1, 'istakakarya.png'),
(13, 'Nindya', 1, 'nindya.png'),
(14, 'PP', 1, 'pp.jpg'),
(15, 'Waskita', 1, 'waskita.jpg'),
(16, 'Vivere', 3, 'vivere.png'),
(17, 'Gitalaras', 3, 'gita.png'),
(18, 'Caturgriya', 3, 'catur.png'),
(19, 'Romance', 3, 'romance.png'),
(20, 'Indovickers', 3, 'indovickers.png'),
(21, 'Metric', 3, 'metric.png'),
(22, 'Kanefusa', 3, 'kanefusa.png'),
(23, 'Loista', 3, 'loista.png'),
(24, 'Total', 4, 'total.png'),
(25, 'Shimz', 4, 'Shimz.jpg'),
(26, 'Multikon', 4, 'amultikon.jpg'),
(27, 'Leighton', 4, 'aleighton.jpg'),
(28, 'Acset', 4, 'acset.jpg'),
(29, 'Jaya Konstruksi', 4, 'jakon.jpg'),
(30, 'Pulau Intan', 4, 'pulau-intan.jpg'),
(31, 'Beton', 4, 'beton.png'),
(32, 'Totalindo', 4, 'totalindo.png'),
(33, 'Balfour Beatty Sakti', 4, 'ballfour.png'),
(34, 'NRC', 4, 'nrc.jpg'),
(35, 'Bam', 4, 'bam.png'),
(39, 'yyyyyyyaa', 1, '');

-- --------------------------------------------------------

--
-- テーブルの構造 `messages`
--

CREATE TABLE `messages` (
  `id` int(10) NOT NULL,
  `sendername` varchar(100) NOT NULL,
  `sendermail` varchar(100) NOT NULL,
  `senderphone` varchar(100) NOT NULL,
  `sendersub` varchar(100) NOT NULL,
  `sendermsg` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- テーブルのデータのダンプ `messages`
--

INSERT INTO `messages` (`id`, `sendername`, `sendermail`, `senderphone`, `sendersub`, `sendermsg`) VALUES
(1, 'IIN FIRLYANA', 'chrome.sama88@gmail.com', '083878378473', 'yyy', 'ok');

-- --------------------------------------------------------

--
-- テーブルの構造 `product`
--

CREATE TABLE `product` (
  `id` int(2) NOT NULL,
  `name_product` varchar(30) NOT NULL,
  `descriptions` text NOT NULL,
  `picture` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- テーブルのデータのダンプ `product`
--

INSERT INTO `product` (`id`, `name_product`, `descriptions`, `picture`) VALUES
(1, 'Ordinary Plywood', '<pre><strong>1. Terbuat Dari :</strong><br />- Kayu alam ( Kamper, Meranti, Kruing )<br />- Kayu HTI ( Albasia, Karet, Sengon )<br /><br /><strong>2. Permukaan Polos<br /></strong><br /><strong>3. Tipe :</strong><br />- Ordinary Plywood<br />- Block Board<br /><br /><strong>4. Ketebalan :</strong><br />- 3mm - 18mm<br /><br /><strong>5. Fungsi :</strong><br />- Interior dan Furniture<br />- Konstruksi bangunan</pre>', '70HASDI-18.jpg'),
(2, 'Film Face Plywood', '<pre><strong>1. Plywood yang dilapisi dengan </strong><br /><strong>lembaran Phenol Formaldehyde Film </strong><br /><strong>(45 / 125 gsm) pada satu sisi atau dua sisi</strong><br /><br /><strong>2. Daya ulang pemakaian :</strong><br />- 8 &ndash; 10 kali pakai<br /><br /><strong>3. Memberi hasil permukaan beton yang licin </strong><br /><strong>dan merata, dengan daya pemakaian berulang kali</strong> <br /><br /><strong>4. KETEBALAN :</strong><br />- 3mm &ndash; 18mm<br /><br /><strong>5. FUNGSI :</strong><br />- Industri Furniture<br />- Konstruksi bangunan ( Form Work )</pre>', '21HASDI-17.jpg'),
(4, 'Poly Plywood', '<pre><strong>1. Permukaan plywood dilapisi dengan <br />cairan Poly Resin<br />\r\n2. Warna</strong>\r\n- Transparan - Hitam - Abu &ndash; Abu\r\n<strong><br />3. Daya ulang pemakaian</strong>\r\n- 3 &ndash; 5 kali pakai\r\n<strong><br />4. Memberi hasil permukaan beton yang licin\r\n<br />5. Ketebalan :</strong>\r\n- 9mm &ndash; 18mm\r\n<strong><br />6. Fungsi :</strong>\r\n- Industri Furniture\r\n- Konstruksi bangunan ( Form Work )</pre>', '21HASDI-15.jpg'),
(6, 'Fancy Plywood', '<pre><strong>1. Satu muka dilapisi dengan Veneer Kayu</strong><br /><br /><strong>2. Polyester :</strong><br />- Melaminto Doof / Polywood Kilap<br /><br /><strong>3. TIPE :</strong><br />- Ordinary Plywood<br />- Block Board<br /><br /><strong>4. KETEBALAN :</strong><br />- 3mm<br /><br /><strong>5. FUNGSI :</strong><br />- Interior dan Furniture</pre>', '2HASDI-16.jpg'),
(7, 'Building Materials', '<pre><strong>1. Laminated Veneer Lumber &amp; <br />Laminated Veneer Board </strong><br />- LVL : arah seratnya searah<br />- LVB : arah seratnya tidak searah<br /><br /><strong>2. Ukuran presisi</strong> <br /><br /><strong>3. Kekuatan merata</strong><br /><br /><strong>4. MOR dan MOE dapat diperhitungkan</strong> <br /><br /><strong>5. MC stabil : &plusmn; 14%</strong> <br /><br /><strong>6. Anti Serangga &amp; Jamur</strong></pre>', '34HASDI-14.jpg'),
(8, 'Yoshino Gypsum', '<pre><strong>1. Yoshino Gypsum Board Regular</strong><br />- Tebal : 9 mm<br />- Ukuran : 1200 x 2400 mm<br />- Berat : +/- 16 Kg/lembar<br /><br /><strong>2. Yoshino Gypsum Board Regular</strong><br />- Tebal : 9 mm<br />- Ukuran : 1200 x 2400 mm<br />- Berat : +/- 16 Kg/lembar</pre>', '97HASDI-13.jpg');

-- --------------------------------------------------------

--
-- テーブルの構造 `project`
--

CREATE TABLE `project` (
  `id` int(6) NOT NULL,
  `name_project` varchar(100) NOT NULL,
  `years` varchar(4) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- テーブルのデータのダンプ `project`
--

INSERT INTO `project` (`id`, `name_project`, `years`) VALUES
(1, 'PIK Mall - Pantai Indah Kapuk', '2014'),
(2, 'Sahid Sudirman Centre - Jakarta', '2014'),
(3, 'Ciputra World 2 Mall - Jakarta', '2014'),
(4, 'Graha MRA - Jakarta', '2014'),
(5, 'Australia Embassy - Jakarta', '2014'),
(6, 'Matahari Tower - Jakarta', '2014'),
(7, 'Springhill Kemayoran - Jakarta', '2014'),
(8, 'Pluit Sea View - Jakarta', '2014'),
(9, 'Apartemen Sudirman Suites - Jakarta', '2014'),
(10, 'Apartemen Park View Bintaro - Jakarta', '2014'),
(11, 'Astra Toyota - Jakarta', '2014'),
(12, 'Pergudangan Cikarang - Cikarang', '2014'),
(13, 'Tol Pelumpang - Jakarta', '2014'),
(14, 'Sentul City - Bogor', '2014'),
(15, 'Cleuterep Pabrik Holsim - Bogor', '2014'),
(16, 'Bandara Soekarno Hatta - Jakarta', '2014'),
(17, 'AEON Mall - Tangerang', '2014'),
(18, 'Bogor Green Forest - Bogor', '2014'),
(19, 'Serpong Green View - Serpong', '2014'),
(20, 'Rusunawa Tower B - Jatinegara', '2014'),
(21, 'Tol Cikampek-Palimanan - Subang', '2014'),
(22, 'Jembatan Ir. Manado - Manado', '2014'),
(23, 'Tower Izzar - Simatupang', '2014'),
(24, 'Pabrik Kertas - Jambi', '2014'),
(25, 'Jakarta Fair 2014 - Kemayoran', '2014'),
(26, 'Perumahan Green Lake - Kalideres', '2014'),
(27, 'Apartemen Four Wind - Permata Hijau', '2014'),
(28, 'Tower Galery West - Kebon Jeruk', '2014'),
(29, 'Hotel Saffron - Bandung', '2014'),
(30, 'Bandung Electronic Center - Bandung', '2014'),
(31, 'Apartemen U\'Residence - Karawaci', '2014'),
(32, 'Djarum Kudus - Jawa Tengah', '2014'),
(33, 'The Prominence Office Tower - Alam Sutera', '2014'),
(34, 'Tower ALTIRA - Cempaka Putih', '2014');

-- --------------------------------------------------------

--
-- テーブルの構造 `project_best`
--

CREATE TABLE `project_best` (
  `id` int(2) NOT NULL,
  `best_project` varchar(100) NOT NULL,
  `picture` varchar(50) NOT NULL,
  `years` varchar(4) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- テーブルのデータのダンプ `project_best`
--

INSERT INTO `project_best` (`id`, `best_project`, `picture`, `years`) VALUES
(1, 'Australia Embassy - Jakarta', 'HASDI-07.jpg', '2014'),
(2, 'AEON Mall - Tangerang', 'HASDI-08.jpg', '2014'),
(3, 'PIK Mall - Pantai Indah Kapuk', 'HASDI-09.jpg', '2014');

-- --------------------------------------------------------

--
-- テーブルの構造 `tahun`
--

CREATE TABLE `tahun` (
  `id` int(4) NOT NULL,
  `project_year` varchar(4) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- テーブルのデータのダンプ `tahun`
--

INSERT INTO `tahun` (`id`, `project_year`) VALUES
(1, '2014'),
(2, '2013'),
(3, '2012'),
(4, '2011'),
(5, '2010'),
(6, '2015'),
(7, '2016'),
(8, '2017'),
(9, '2018');

--
-- ダンプしたテーブルのインデックス
--

--
-- テーブルのインデックス `about`
--
ALTER TABLE `about`
  ADD PRIMARY KEY (`id`);

--
-- テーブルのインデックス `admins`
--
ALTER TABLE `admins`
  ADD PRIMARY KEY (`username`);

--
-- テーブルのインデックス `banner`
--
ALTER TABLE `banner`
  ADD PRIMARY KEY (`id`);

--
-- テーブルのインデックス `category_client`
--
ALTER TABLE `category_client`
  ADD PRIMARY KEY (`kode`);

--
-- テーブルのインデックス `client`
--
ALTER TABLE `client`
  ADD PRIMARY KEY (`id`);

--
-- テーブルのインデックス `messages`
--
ALTER TABLE `messages`
  ADD PRIMARY KEY (`id`);

--
-- テーブルのインデックス `product`
--
ALTER TABLE `product`
  ADD PRIMARY KEY (`id`);

--
-- テーブルのインデックス `project`
--
ALTER TABLE `project`
  ADD PRIMARY KEY (`id`);

--
-- テーブルのインデックス `project_best`
--
ALTER TABLE `project_best`
  ADD PRIMARY KEY (`id`);

--
-- テーブルのインデックス `tahun`
--
ALTER TABLE `tahun`
  ADD PRIMARY KEY (`id`);

--
-- ダンプしたテーブルのAUTO_INCREMENT
--

--
-- テーブルのAUTO_INCREMENT `about`
--
ALTER TABLE `about`
  MODIFY `id` int(2) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- テーブルのAUTO_INCREMENT `banner`
--
ALTER TABLE `banner`
  MODIFY `id` int(2) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- テーブルのAUTO_INCREMENT `category_client`
--
ALTER TABLE `category_client`
  MODIFY `kode` int(2) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- テーブルのAUTO_INCREMENT `client`
--
ALTER TABLE `client`
  MODIFY `id` int(6) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=40;

--
-- テーブルのAUTO_INCREMENT `messages`
--
ALTER TABLE `messages`
  MODIFY `id` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- テーブルのAUTO_INCREMENT `product`
--
ALTER TABLE `product`
  MODIFY `id` int(2) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=13;

--
-- テーブルのAUTO_INCREMENT `project`
--
ALTER TABLE `project`
  MODIFY `id` int(6) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=37;

--
-- テーブルのAUTO_INCREMENT `project_best`
--
ALTER TABLE `project_best`
  MODIFY `id` int(2) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=15;

--
-- テーブルのAUTO_INCREMENT `tahun`
--
ALTER TABLE `tahun`
  MODIFY `id` int(4) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
