<!DOCTYPE html>
<html>
    <head>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width"><link rel='stylesheet' href='https://npmcdn.com/flickity@2/dist/flickity.css'>
        <link rel="stylesheet" type="text/css" href="styles.css">
        <TItle> PT. Hasdi Mustika Utama 2019</TItle>
    </head>
    <header>
        
    </header>
    <body>
        <div class="header" style="position: relative;">
            <div class="logo">
                <img class="logo"src="assets/logo-01.jpg" alt="logo">
            </div>
            <div class="header-right" id="myTopnav">
                <a href="index.php">HOME</a>
                <a href="articles.php">ARTICLES</a>
                <a href="#" class="active">CLIENT</a>
                <a href="product.php">PRODUCT</a>
                <a href="project.php">PROJECT</a>
                <a href="about.php">ABOUT US</a>
                <a href="contact.php">CONTACT US</a>
                <a href="javascript:void(0);" style="font-size:1.1vw;" class="icon" onclick="myFunction()">&#9776;</a>
            </div>
        </div>
        <div class="content" style="padding: 2% 17%;">
            <h1>CLIENT</h1>
            <p>Goverment Contractors :</p>

            <?php
            include "adm/config/koneksi.php";
            $tampil=mysqli_query($conn,"SELECT * FROM client WHERE category=1 ORDER BY id DESC");
            $no=1;
            while ($r=mysqli_fetch_array($tampil))	
            {?>
                <img class="client-img" src="adm/gambar_klien/<?php echo $r['picture'];?>" alt="">
            <?php
            $no++;
            }?>

        </div>
        <div class="content" style="padding: 2% 17%;">
            <p>Interior Contractors :</p>
            <?php
            include "adm/config/koneksi.php";
            $tampil=mysqli_query($conn,"SELECT * FROM client WHERE category=3 ORDER BY id DESC");
            $no=1;
            while ($r=mysqli_fetch_array($tampil))	
            {?>
                <img class="client-img" src="adm/gambar_klien/<?php echo $r['picture'];?>" alt="">
            <?php
            $no++;
            }?>
        </div>
        <div class="content" style="padding: 2% 17%;">
            <p>Private Contractors :</p>
            <?php
            include "adm/config/koneksi.php";
            $tampil=mysqli_query($conn,"SELECT * FROM client WHERE category=4 ORDER BY id DESC");
            $no=1;
            while ($r=mysqli_fetch_array($tampil))	
            {?>
                <img class="client-img" src="adm/gambar_klien/<?php echo $r['picture'];?>" alt="">
            <?php
            $no++;
            }?>
        </div>
        <script src='https://code.jquery.com/jquery-3.1.0.min.js'></script>
        <script src='https://npmcdn.com/flickity@2/dist/flickity.pkgd.js'></script>
        <script  src="script.js"></script>
    </body>
    <footer>
        <img src="assets/HASDI-10.jpg" alt="">
    </footer>
    </html>