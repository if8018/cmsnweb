<!DOCTYPE html>
<html>
    <head>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width"><link rel='stylesheet' href='https://npmcdn.com/flickity@2/dist/flickity.css'>
        <link rel="stylesheet" type="text/css" href="styles.css">
        <TItle> PT. Hasdi Mustika Utama 2019</TItle>
    </head>
    <header>
        
    </header>
    <body>
        <div class="header" style="position: relative;">
            <div class="logo">
                <img class="logo"src="assets/logo-01.jpg" alt="logo">
            </div>
            <div class="header-right" id="myTopnav">
                <a href="index.php">HOME</a>
                <a href="#" class="active">ARTICLES</a>
                <a href="client.php">CLIENT</a>
                <a href="product.php">PRODUCT</a>
                <a href="project.php">PROJECT</a>
                <a href="about.php">ABOUT US</a>
                <a href="contact.php">CONTACT US</a>
                <a href="javascript:void(0);" style="font-size:1.1vw;" class="icon" onclick="myFunction()">&#9776;</a>
            </div>
        </div>
        <div class="content" style="padding: 2% 17%;">
            <h1>KAYU LAPIS</h1>
            <p> Kayu lapis merupakan produk komposit yang terbuat dari lembaran-lembaran vinir yang direkatkan bersama dengan
                susunan bersilangan tegak lurus. Kayu lapis termasuk ke dalam salah satu golongan panel struktural, dimana arah
                penggunaan kayu lapis ini adalah untuk panel-panel struktural. Cikal bakal munculnya kayu lapis terjadi
                di Mesir sekitar tahun 1500 S.M dimana pada masa tersebut orang-orang Mesir telah mampu membuat vinir untuk
                menghiasi perabot rumah tangga mereka. Selanjutnya disusul bangsa Yunani dan Roma kuno mengembangkan alat
                pemotong vinir (Haygreen dan Bowyer, 1993). Seiring dengan meningkatnya kebutuhan bahan konstruksi maka
                keberadaan industri kayu lapis mulai berkembang. <br><br>
                Kayu Lapis atau biasa disebut plywood (tripleks untuk kayu lapis dengan 3 lapisan, dan multipleks untuk kayu lapis
                dengan 5 lapisan atau lebih), menurut definisi para ahli adalah sebagai berikut: <br><br>
                Kayu Lapis adalah suatu produk yang diperoleh dengan cara menyusun bersilangan tegak lurus bersilangan lembaran
                vinir yang diikat dengan perekat, minimal 3 (tiga) lapis (SNI, 1992). <br><br>
                Tsoumis (1991) mengemukakan bahwa, kayu lapis adalah produk panel yang terbuat dengan merekatkan sejumlah
                lembaran vinir atau merekatkan lembaran vini pada kayu gergajian, dimana kayu gergajian sebagai bagian intinya/core
                (yang lebih dikenal sebagai wood core plywood). Arah serat pada lembaran vinir untuk face dan core adalah saling
                tegak lurus, sedangkan antar lembaran vinir untuk face saling sejajar. <br><br>
                Youngquist (1999) mengemukakan bahwa kayu lapis merupakan panel datar yang tersusun atas lembaran-lembaran
                vinir yang disatukan oleh bahan pengikat (perekat) dibawah kondisi pengempaan. <br><br>
                Haygreen dan Bowyer (1993) mengemukakan bahwa kayu lapis merupakan produk panel vinir-vinir kayu yang direkat
                bersama sehingga arah serat sejumlah vinirnya tegak lurus dan yang lainnya sejejar sumbu panjang panil.
            </p>
        </div>
        <script src='https://code.jquery.com/jquery-3.1.0.min.js'></script>
        <script src='https://npmcdn.com/flickity@2/dist/flickity.pkgd.js'></script>
        <script  src="script.js"></script>
    </body>
    <footer>
        <img src="assets/HASDI-10.jpg" alt="">
    </footer>
    </html>