<!DOCTYPE html>
<html>
    <head>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width"><link rel='stylesheet' href='https://npmcdn.com/flickity@2/dist/flickity.css'>
        <link rel="stylesheet" type="text/css" href="styles.css">
        <TItle> PT. Hasdi Mustika Utama 2019</TItle>
    </head>
    <header>
        
    </header>
    <body>
        <div class="header" style="position: relative;">
            <div class="logo">
                <img class="logo"src="assets/logo-01.jpg" alt="logo">
            </div>
            <div class="header-right" id="myTopnav">
                <a href="index.php">HOME</a>
                <a href="articles.php">ARTICLES</a>
                <a href="client.php">CLIENT</a>
                <a href="#" class="active">PRODUCT</a>
                <a href="project.php">PROJECT</a>
                <a href="about.php">ABOUT US</a>
                <a href="contact.php">CONTACT US</a>
                <a href="javascript:void(0);" style="font-size:1.1vw;" class="icon" onclick="myFunction()">&#9776;</a>
            </div>
        </div>
        <div class="content" style="padding: 2% 17%;">
            <h1>Product</h1>
        </div>

        <?php
            include "adm/config/koneksi.php";
            $tampil=mysqli_query($conn,"SELECT * FROM product ORDER BY id");
        $no=1;
        while ($r=mysqli_fetch_array($tampil))	
        {?>
         <div class="content" style="padding: 2% 15%;">
            <img class="about-img" src="adm/gambar_produk/<?php echo $r['picture'];?>" alt="">
            <div class="description">
            <h2><?php echo $r['name_product'];?></h2>
            <div class="desc"><?php echo $r['descriptions'];?></div>
            </div>
        </div>
        <?php
        $no++;
        }?>
        
        <script src='https://code.jquery.com/jquery-3.1.0.min.js'></script>
        <script src='https://npmcdn.com/flickity@2/dist/flickity.pkgd.js'></script>
        <script  src="script.js"></script>
    </body>
    <footer>
        <img src="assets/HASDI-10.jpg" alt="">
    </footer>
    </html>