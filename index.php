<!DOCTYPE html>
<html>
    <head>
        <meta name="viewport" content="width=device-width, initial-scale=1" charset="UTF-8">
        <link rel="stylesheet" type="text/css" href="styles.css">
        <meta name="viewport" content="width=device-width"><link rel='stylesheet' href='https://npmcdn.com/flickity@2/dist/flickity.css'>
        <TItle> PT. Hasdi Mustika Utama 2019</TItle>
    </head>
    <header>
        
    </header>
    <body>
                <?php
                include "adm/config/koneksi.php";
                $tampil=mysqli_query($conn,"SELECT * FROM banner");
                $no=1;
                while ($r=mysqli_fetch_array($tampil))	
                {?>
        <div class="hero-image" style="background-image: linear-gradient(rgba(0, 0, 0, 0.5), rgba(0, 0, 0, 0.5)), url('adm/gambar_banner/<?php echo $r['picture'];?>');">
                <?php
                $no++;
                }?>
            <div class="header">
                <div class="logo">
                    <img class="logo"src="assets/logo-01.jpg" alt="logo">
                </div>
                <div class="header-right" id="myTopnav">
                    <a href="#home" class="active">HOME</a>
                    <a href="articles.php">ARTICLES</a>
                    <a href="client.php">CLIENT</a>
                    <a href="product.php">PRODUCT</a>
                    <a href="project.php">PROJECT</a>
                    <a href="about.php">ABOUT US</a>
                    <a href="contact.php">CONTACT US</a>
                    <a href="javascript:void(0);" style="font-size:1.1vw;" class="icon" onclick="myFunction()">&#9776;</a>
                </div>
            </div>
            <div class="hero-text">
                <h1>THE RIGHT PARTNER, THE RIGHT PRODUCT AND THE RIGHT DECISION.</h1>
                <br>
                <a class="more" href="contact.php" style="text-decoration: none;">CONTACT US</a>
            </div>
        
        </div>
        <div class="grid">
            <div class="grid-content">
                <img src="assets/HASDI-01.jpg" alt="">
                <div class="overlay">
                    <div class="text">Ordinary Plywood</div>
                </div>
            </div>
            <div class="grid-content">
                <img src="assets/HASDI-02.jpg" alt="">
                <div class="overlay">
                    <div class="text">Film Face Plywood</div>
                </div>
            </div>
            <div class="grid-content">
                <img src="assets/HASDI-03.jpg" alt="">
                <div class="overlay">
                    <div class="text">Fancy Plywood</div>
                </div>
            </div>
            <div class="grid-content">
                <img src="assets/HASDI-04.jpg" alt="">
                <div class="overlay">
                    <div class="text">Poly Plywood</div>
                </div>
            </div>
            <div class="grid-content">
                <img src="assets/HASDI-05.jpg" alt="">
                <div class="overlay">
                    <div class="text">LVL/LVB</div>
                </div>
            </div>
            <div class="grid-content">
                <img src="assets/HASDI-06.jpg" alt="">
                <div class="overlay">
                    <div class="text">Yoshino Gypsum</div>
                </div>
            </div>
        </div>
        <div class="content" style="padding-top: 10vw;">
            <img class="about-img" src="assets/HASDI-11.jpg" alt="">
            <div class="abo">
                <h1>ABOUT US</h1>
                <p> Berdiri sejak tahun 1978, PT. Hasdi Mustika Utama adalah distributor
                    plywood dan pintu Homen di Indonesia yang telah memenuhi dan berpredikat standard ISO Management Mutu 9001. Hingga saat ini PT
                    Hasdi Mustika Utama telah banyak bekerjasama dengan Perusahaan-Perusahaan Swasta, Asing maupun BUMN terutama dalam hal
                    pengadaan Plywood dan Pintu baja bermutu untuk pembangunan
                    berbagai Mega proyek di indonesia.
                </p>
                <a class="more" href="about.php" style="text-decoration: none;">READ MORE</a>
            </div>
        </div>
        <div class="content" style="padding-top: 8vw;">
            <h1>PROJECT</h1>
            <div class="button-row">
                <div class="button-group">
                    <?php
                    include "adm/config/koneksi.php";
                    $tampil=mysqli_query($conn,"SELECT * FROM tahun ORDER BY project_year DESC");
                    $no=1;
                    while ($r=mysqli_fetch_array($tampil))	
                    {?>
                    <button class="button"><?php echo $r['project_year'];?></button>
                    <?php
                    $no++;
                    }?>
                </div>
            </div>
            <div class="carousel">
                <?php
                include "adm/config/koneksi.php";
                $tampil=mysqli_query($conn,"SELECT * FROM project_best WHERE years='2014' ORDER BY id DESC");
                $no=1;
                while ($r=mysqli_fetch_array($tampil))	
                {?>
                <div class="carousel-cell">
                <img src="adm/gambar_project/<?php echo $r['picture'];?>" alt="">
                <h1><?php echo $r['best_project'];?></h1>
                </div>
                <?php
                $no++;
                }?>
            </div>
        </div>
        <div class="content" style="padding-top: 10vw;">
            <h1>CLIENT</h1>
            <p>Goverment Contractors :</p>
            <?php
            include "adm/config/koneksi.php";
            $tampil=mysqli_query($conn,"SELECT * FROM client WHERE category=1 ORDER BY id DESC");
            $no=1;
            while ($r=mysqli_fetch_array($tampil))	
            {?>
                <img class="client-img" src="adm/gambar_klien/<?php echo $r['picture'];?>" alt="">
            <?php
            $no++;
            }?>
        </div>
        <script src='https://code.jquery.com/jquery-3.1.0.min.js'></script>
        <script src='https://npmcdn.com/flickity@2/dist/flickity.pkgd.js'></script>
        <script  src="script.js"></script>
    </body>
    <footer>
        <img src="assets/HASDI-10.jpg" alt="">
    </footer>
    </html>